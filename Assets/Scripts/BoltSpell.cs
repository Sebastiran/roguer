﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltSpell : MonoBehaviour
{
    public Bolt boltInfo;

    public void Cast()
    {
        GetComponent<Rigidbody>().velocity = (boltInfo.targetPosition - boltInfo.startPosition).normalized * boltInfo.speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Entity>())
        {
            other.gameObject.GetComponent<Entity>().DoDamage(boltInfo.damage);
        }
    }
}