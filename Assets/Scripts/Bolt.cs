﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bolt : MonoBehaviour
{
    public float damage = 10f;
    public float manaCost = 0f;
    public Vector3 startPosition, targetPosition;
    public GameObject boltPrefab;
    public float speed = 20f;

    private bool charged = false;

    private void Update()
    {
        if (Input.GetKeyDown("1"))
        {
            charged =! charged;
        }

        if (charged)
        {
            Vector2 mouse = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            Ray ray;
            ray = Camera.main.ScreenPointToRay(mouse);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                targetPosition = hit.point;
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            if(charged)
            {
                startPosition = transform.position;
                targetPosition.y = startPosition.y;
                GameObject bolt = Instantiate(boltPrefab, startPosition, Quaternion.identity, null);
                bolt.GetComponent<BoltSpell>().boltInfo = this;
                bolt.GetComponent<BoltSpell>().Cast();
                charged = false;
            }
        }
    }
}

/*public interface ISpellComponent
{
}

public interface IShape : ISpellComponent
{
}

public interface IEffect : ISpellComponent
{
}

public class Projectile : IShape
{

}

public class Spell
{
    List<IShape> shapes = new List<IShape>();
    List<IEffect> effects = new List<IEffect>();
    //Modifiers[] modifiers = new Modifiers[];

    public Spell(ISpellComponent)
    {

    }
}

public class SpellBook
{
    List<Spell> spells = new List<Spell>();


    //public Spell fireBolt = new Damage(new FireDamage(new Projectile()));
}

public class SpellForger
{
    public void CreateSpell()
    {
        Spell newSpell = new Spell();
    }
}*/